
// #############################################################################
//Escrito por Miguel Gomez en 6/6/16
//#Este programa prende y apaga el RELE_LUCES segun los programas definidos en el array programas
//Agregando programas de luces en eeprom( 5 programas con 4 datos c/u) 20 bytes
//que se cuentan desde el 10 al 29. c/prog cuenta en este orden: Hora de inicio, minutos de inicio
//Hora de apagado y minutos de apagado-listo


//Agregando enlace inhalambrico nrf24l01- Un maestro escucha en 5 canales, y hasta 5 esclavos pueden
//enviar datos. el maestro es la direccion 0, los esclavos  de 1 a 5 .-postergado

//Agregando entrada de botonera para encender el RELE_SINFIN y el RELE_LUCES
//El cableado se realizara de masa a boton de encendido, de la salida correspondiente a boton de apagado,con r=10k
//y la salida restante de cada boton a la entrada correspondiente en el arduino.
//la logica de los rele es negada y la de la entrada tambien.EL rele se enciende cuando 
//arduino envia masa y arduino enciende el rele cuando recibe masa en la entrada.--listo

//agregando sensor reflectivo para detectar el llenado de carro comedero-listo
//agregando DHT11 o 22 para medir temperatura y humedad-postergado
//Cambiando la placa base a MEGA -listo 7/8/18
//Agregado LCD 16x2--listo 9/8/18
//Agregando menues a la pantalla LCD--en proceso 10/8/18
//Agregando Menu Serial:
//mostrar programas-listo
//modificar la hora actual -listo
//modificar un programa de luces -listo
////---por hacer---///
//modificar la fecha actual--
//modificar la iluminacion del display
//modificar el contraste del display
//autoconfigurar los niveles de activacion del sensor IR
//Agegado WDT 30/8/18-hecho


//Eliminando la interrupcion c/1 seg del RTC - cambiando control a una consulta p/segundo por parte del controlador- en proceso 20/7/18-terminado y testeado ok 3/12/18


//Conexiones:
//rf24l01 -Arduino  Las patas del rf se cuentan desde el pin 1 en orden inverso al reloj
//    1-----gnd
//    2-----D9
//    3-----D13
//    4-----D12
//    5-----NC
//    6-----D11
//    7-----D10
//    8-----3V3
//tinyRTC--Arduino
//   Sq-----D2//eliminado --se usaba para la interrupcion del reloj 
//   DS-----D4(opcional)//eliminado para ds3231-probar en ds1307 y eliminar en lo posible
//   SCL----A5-21(MEGA)
//   SDA----A4-20(MEGA)
//   GND-----gnd
//   VCC-----5V
//Relesx2----Arduino//Reles x4 agregar en D4 y D6
//    In1----D3
//    In2----D4
//    GND----gnd
//    VCC----5V
//    In3----D5
//    In4----D6
//Display LCd 16x2
//Vo(contraste)------9
//Anodo Backlight(+)-10
//Rs-----------------22
//E------------------24
//D4-----------------26
//D5-----------------28
//D6-----------------30
//D7-----------------32
//                                                                                   x
//Entradas DB9
//    1 y 9--------------masa
//    2-Termico sinfin---- 49
//    3-Btn luces------ ---48
//    4-Btn_off sinfin---- 51
//    5-Btn_on sinfin----- 50
//    6-Btn bomba----------53
//    7--------------------52
//    8--------------------47
//
//Entradas jack 4Pin
//    1-5V
//    2-SENSOR_IR llenado---A2
//    3-SENSOR_IR alimentacion-46
//    4-Masa
//
//Entradas jack 3Pin
//    1-5V
//    2-dht11-----------8
//    3-Masa
//
//Uso de la memoria EEPROM
//10-13 Programa luces 0 (hh_on,mm_on,hh_off,mm_off)
//14-17 Programa luces 1
//18-21 Programa luces 2
//21-24 Programa luces 3
//25-29 Programa luces 4
//31-32 nivel_sensor-(0-1023)voltaje por debajo del cual se activa el sensor IR

// #############################################################################
// *********************************************
// INCLUDE
// *********************************************
#include <Wire.h>                       // For some strange reasons, Wire.h must be included here
#include <DS1307new.h>//libreria que sirve para ds3231 , pero no con setRam y getRam//Usar la version ds1307newAlarms de Milé Buurmeijer
#include <SPI.h>
//#include "RF24.h"
#include <EEPROM.h>
#include <stdlib.h>
#include <DHT.h>//Usar la version de adafruit y agregar la libreria Adafruit Unified Sensor
#include <TimerOne.h>
#include <LiquidCrystal.h>
#include <avr/wdt.h>
// *********************************************
// DEFINE
// *********************************************



#define programareloj 0 // '1' para programar; '0' para no programar
//    completar los datos deseados//
#define dia 31
#define mes 12
#define ano 2018
#define hora 19
#define minuto 29
#define segundo 00

//pines
#define DHT_PIN 8 //pin con la señal del sensor DHT 11
#define RELE_LUCES 3 //usado para las luces **en versiones anteriores es 5
#define RELE_SINFIN 4 //usado para el sinfin de carga **en versiones anteriores es 3
#define RELE_BOMBA 5
#define rele4 6
#define SENSOR_IR_LLENADO A2//señal del SENSOR_IR
#define SENSOR_IR_ALIM 46
#define BOTON_ON_SINFIN 50
#define BOTON_OFF_SINFIN 51
#define TERM_SINFIN 49
#define BOTON_LUCES 48
#define BOTON_BOMBA 53
#define PWM_CONTRASTE_LCD 9
#define PWM_ILUMINACION_LCD 10
#define LCD_D7 32
#define LCD_D6 30
#define LCD_D5 28
#define LCD_D4 26
#define LCD_E  24
#define LCD_RS 22

//Expresiones usuales
#define DESEA_CONFIRMAR "Desea confirmar? (S/n)"
#define ACTUALIZANDO "Actualizando..."
#define ABORTANDO "Abortando"
#define INGRESE_HORA "Ingrese una hora válida.(0-23)"
#define INGRESE_MINUTOS "Ingrese minutos válidos.(0-59)"

#define VAL_SENSOR_SUP 31//byte mas significatico del valor de corte del sensor
#define VAL_SENSOR_INF 32//byte menos significativo del valor de corte del sensor

#define DHT_TYPE 11 //definir que tipo de sensor.Las otras opciones son 21 y 22
/*******************
 * CLASES
 ********************/

class programa{
  public:
    serial(void);
    programa(unsigned char n_programa);
    unsigned char num_prog;
    unsigned int hora_encendido;
    unsigned int hora_apagado;
    boolean es_mismodia;
    void inciaVariables(void);
    boolean estadoLuces(unsigned int hora_actual);
    String most_hora(int num);
    void setProg(int he,int me,int ha, int ma);
};

programa::serial(void){
  Serial.print(F("Programa Nº:"));
  Serial.println(num_prog);
  Serial.print(F("Encendido: "));
  Serial.print(most_hora(hora_encendido/60));
  Serial.print(':');
  Serial.println(most_hora(hora_encendido%60));
  Serial.print(F("Apagado: "));
  Serial.print(most_hora(hora_apagado/60));
  Serial.print(':');
  Serial.println(most_hora(hora_apagado%60));
}

programa::programa(unsigned char n_programa){
  if (n_programa<1 || n_programa>5){
    Serial.println(F("Numero de programa Incorrecto. Elija 1 a 5."));
  }else{
    num_prog=n_programa;
    hora_encendido=EEPROM.read(n_programa*4+6)*60+EEPROM.read(n_programa*4+7);
    hora_apagado=EEPROM.read(n_programa*4+8)*60+EEPROM.read(n_programa*4+9);
    if(hora_encendido<=hora_apagado)es_mismodia=1;else es_mismodia=0;
  }
}
boolean programa::estadoLuces(unsigned int hora_actual){
  if(es_mismodia){
    if(hora_actual>hora_encendido&&hora_actual<hora_apagado){
      return 1;
    }
  }else{
    if(hora_actual<hora_apagado||hora_actual>hora_encendido){
      return 1;
    }
  }
  return 0;
}
void programa::setProg(int he,int me,int ha,int ma){
  EEPROM.update(num_prog*4+6,he);
  EEPROM.update(num_prog*4+7,me);
  EEPROM.update(num_prog*4+8,ha);
  EEPROM.update(num_prog*4+9,ma);
  hora_encendido=he*60+me;
  hora_apagado=ha*60+ma;
    if(hora_encendido<=hora_apagado)es_mismodia=1;else es_mismodia=0;
}

//*********************************************************//
//Funcion que recibe un entero y devuelve un String arreglado para mostrar dos digitos
//*********************************************************//

String programa::most_hora(int num){
  if (num<10){
    return String("0"+ String(num));
  }else{
    return String(num);
  }
  
  
}

///******************************************////
//**********FIN DE CLASE PROGRAMA***/////////////
/////////////////////////////////////////////////

// *********************************************
// VARIABLES
// *********************************************

unsigned char estado_bomba=0;//establece el estado de los aspersores (0 apagado)
unsigned char secuencias_bomba[4][2]={{15,30},{15,45},{20,45},{20,60}};
unsigned char contador_bomba=0;
int contador_ciclos=0;
int radioNumber = 0; //usado como direccionMAC del enlace inalambrico --no implementado
byte direcciones[][7] = {"Master", "1Nodo", "2Nodo", "3Nodo", "4Nodo", "5Nodo", "6Nodo" };//direcciones del enlace --no implementado
unsigned char programas[20] = {00, 00, 00, 40, 06, 30, 8, 00, 18, 00, 22, 30, 00, 00, 00, 00, 00, 00, 00, 00};//programas de luces --cambiar a EEPROM 20/7/18--hecho, estos valores son obsoletos
unsigned char ultimo_segundo=0;//usado para encontrar el cambio de minuto
unsigned char cont_menu=0; //usada para contar los segundos que pasan mientras se muestra el menu
unsigned char menu_op=0; //usada para determinar en que opcion del menu se navega
unsigned char cont_display=0;//usada para variar la informacion mostrada
int nuevo_minuto,nueva_hora,nuevo_minuto_a,nueva_hora_a,num_p;//variables usadas para la modificacion de variables en el menu
unsigned int nivel_sensor=450;//usado para determinar el nivel de voltaje bajo el que se considera activado el sensor IR
boolean sinfin_modo_manual=0;//bandera para activar el modo manual del sinfin sin leer el sensor IR
boolean luces_modo_manual=0;
boolean boton_on_sinfin_ant=0;//para saber si ya estaba presionado el boton
boolean boton_off_sinfin_ant=0;//para saber si ya estaba presionado el boton
unsigned char boton_off_sinfin_cont=0; //contador para los segundo de apretado del boton OFF_sinfin
unsigned char boton_on_sinfin_cont=0; //contador para los segundo de apretado del boton ON_sinfin
//unsigned char auto_config_ir=0;//estado de la configuracion del sensor IR de llenado
unsigned char auto_config_cont=0;//contador para la autoconfiguracion del sensor;
unsigned char esta_sinfin=4;
/*variables para estado de las entradas*/
boolean est_boton_on_sinfin=0;
boolean est_boton_off_sinfin=0;
boolean est_term_sinfin=0;
boolean est_boton_luces=0;
boolean est_sensor_ir_llenado=0;
boolean est_boton_bomba=0;
unsigned int val_sensor_llenado=0;
unsigned int val_sensor_llenado_a=0;
unsigned int val_sensor_llenado_c=0;
boolean conf_btn_off=false;
/*variables banderas para entradas */
boolean ba_boton_on_sinfin = 0;
boolean ba_boton_off_sinfin=0;
boolean ba_term_sinfin=0;
boolean ba_boton_luces=0;
boolean boton_luces_suelto=0;
boolean ba_sensor_ir_llenado=0;
boolean ba_boton_bomba=0;
boolean boton_bomba_suelto=0;
/*variables para el estado de las salidas*/
boolean est_rele_bomba=0;

boolean en_menu=false;
boolean cada_segundo=false; //usado en loop para ejecutar cada 1 seg -consulta de rtc y temp
boolean cada_100ms=false; //usado en loop para ejecutar cada 100 ms -control de entradas
LiquidCrystal lcd(LCD_RS,LCD_E,LCD_D4,LCD_D5,LCD_D6,LCD_D7);
unsigned char pwm_contraste_lcd=60;
unsigned char pwm_iluminacion_lcd=255;
String hora_lista,dia_listo;
const char imprimir_lcd[5][5]={"Func.","Lleno","!Term","Apag.","NoIni"};

DHT dht(DHT_PIN,DHT_TYPE);

///************************************///
///Manejo de la interrupcion del Timer 1//
///************************************///
void t1_interrupt(void){//Función que se ejecuta con la interrupcion del TimerOne (cada 100ms)
  cada_100ms=true;
  }
//*************************************************************************
//Rutina de configuracion
//**********************************************************************


void setup()
{
 // RF24 radio(9, 10); inicializacion del enlace inalamabrico--no implementado
  delay(500);
  
  pinMode(DHT_PIN,INPUT);
  pinMode(BOTON_ON_SINFIN, INPUT); //boton para encender el sinfin
  pinMode(BOTON_OFF_SINFIN, INPUT);//boton para apagarlo
  pinMode(TERM_SINFIN, INPUT);//señal del relevo termico del motor
  pinMode(SENSOR_IR_LLENADO,INPUT);//señal del sensor de barrera que indica carro lleno
  pinMode(SENSOR_IR_ALIM,OUTPUT);//alimentacion del led IR del sensor de llenado, para comprobar que esta conectado
  pinMode(BOTON_LUCES, INPUT);  //pin para encender las luces manualmente
  pinMode(BOTON_BOMBA, INPUT);  //pin para encender la bomba de los aspersores
  pinMode(RELE_LUCES, OUTPUT); //rele que comanda las luces
  pinMode(RELE_SINFIN, OUTPUT); //rele que comanda el llenado del carro
  pinMode(RELE_BOMBA, OUTPUT);//rele que comanda la bomba de los aspersores
  pinMode(rele4, OUTPUT);
  
  
 // pinMode(pinsegundo, INPUT);  // pin D2 como entrada para la interrupcion ---eliminando

  digitalWrite(RELE_LUCES, HIGH); //se levantan los pines de los reles para
  digitalWrite(RELE_SINFIN, HIGH); //evitar que se enciendan
  digitalWrite(RELE_BOMBA, HIGH);
  digitalWrite(rele4, HIGH);
  //activando pullups en las entradas
  digitalWrite(BOTON_OFF_SINFIN, HIGH);
  digitalWrite(TERM_SINFIN, HIGH);
  digitalWrite(BOTON_ON_SINFIN, HIGH);
  digitalWrite(BOTON_LUCES,HIGH);
  digitalWrite(BOTON_BOMBA,HIGH);
  digitalWrite(DHT_PIN,HIGH);
  digitalWrite(SENSOR_IR_ALIM,HIGH);
  analogWrite(PWM_CONTRASTE_LCD,pwm_contraste_lcd);
  analogWrite(PWM_ILUMINACION_LCD,pwm_iluminacion_lcd);
  Serial.begin(9600);
  lcd.begin(16,2);
  lcd.setCursor(0,0);
  lcd.print(F("Iniciando..."));
  wdt_disable();
  wdt_enable(WDTO_2S);
  
  /*
     modificar el define 'programareloj' en 1 para modificar los datos del reloj,
     sino poner en 0 para modificar el programa sin afectar el reloj.
  */
  if (programareloj){
      programarReloj();
  }else{
      RTC.getTime();
  }

  

  // se actualizan los programas en la eeprom de arduino
  //se debera comentar para poder modificar los programas y persistir las modificaciones
//  if (false){
//    for (int i = 0; i < 20; i++) {
//      EEPROM.update(i + 10, programas[i]);
//    }
//  }

  //  //************************************************************************
  //
  //**rellena el array con los programas desde la eeprom
  for (unsigned char i = 10; i < 30; i++) {
    programas[i - 10] = EEPROM.read(i);
    Serial.println(programas[i - 10]);

  }

  

  //**coloca el rele 1 en el estado que corresponde al inicio
  digitalWrite(RELE_LUCES,controlLuces());
  
//*rutina de configuracion del valor del sensor--ejecutar una vez y comentar
//******************************************
//  EEPROM.update(VAL_SENSOR_SUP, 450 / 256);
//  EEPROM.update(VAL_SENSOR_INF, 450 % 256);
//******************************************

  nivel_sensor=EEPROM.read(VAL_SENSOR_SUP)*256+EEPROM.read(VAL_SENSOR_INF);//configura el nivel por debajo del cual se considera activado el sensor
  
  //iniciliza el sensor de temperatura y humedad
  dht.begin();
  
  //Inicializar el TIMER 1
  Timer1.initialize();
  Timer1.attachInterrupt(t1_interrupt,100000);

}
//**FIN SETUP********************
//*******************************

// *********************************************
// MAIN (LOOP)
// *********************************************
void loop()
{
  
  if (cada_segundo) {//se ejecuta cada 1 segundo
   
    RTC.getTime();
    Serial.print("estado");
    Serial.println(estado_bomba);
    //*rutina de configuracion del modo manual del sinfin//**
    if (est_boton_on_sinfin){
      if(boton_on_sinfin_ant){
        boton_on_sinfin_cont++;
        if(boton_on_sinfin_cont==3){
          if(sinfin_modo_manual){
            sinfin_modo_manual=false;
            Serial.println(F("Volviendo a control del sinfin por Sensor IR"));
            lcd.setCursor(0,1);
            lcd.print(F("Sinfin Auto"));
          }else{
            sinfin_modo_manual=true;
            Serial.println(F("Pasando el control del sinfin a modo manual"));
            lcd.setCursor(0,1);
            lcd.print(F("Sinfin Manual"));            
          }
        }
      }else{
        boton_on_sinfin_ant=true;
        boton_on_sinfin_cont=0;
      }
    }else{
      boton_on_sinfin_ant=false;
    }

    ////*************************************//////
    ///ejecucion de la rutina de los aspersores///
    if (estado_bomba!=0){//si la funcion de intermitencia esta encendida
      contador_bomba++;
      if (est_rele_bomba){//si la bomba esta encendida
        if (contador_bomba>=secuencias_bomba[estado_bomba-1][0]){//si el contador alcanzo el limite tiempo encendido segun la secuencia elegida
          est_rele_bomba=0;//se apaga la bomba
          contador_bomba=0;//se reinicia el contador
        }
        
      }else{//si la bomba esta apagada
        if (contador_bomba>=secuencias_bomba[estado_bomba-1][1]){//si el contador alcanzo el limite de tiempo apagado segun la secuencia elegida
          est_rele_bomba=1;//se enciende la bomba
          contador_bomba=0;//se reinicia el contador
        }
      }
    }

///****ejecucion cada 1 minuto****//////////////
   
    if (RTC.second < ultimo_segundo ) { //ejecucion cada 1 minuto
      //**rutina de control de los programas de luces//***
      if(!luces_modo_manual)digitalWrite(RELE_LUCES,controlLuces());//si esta en modo automatico
      
    }
    ultimo_segundo=RTC.second;

  //***Rutina de configuracion automatica de los valores del sensor IR//
    if (est_boton_off_sinfin){
      if(boton_off_sinfin_ant){
        boton_off_sinfin_cont++;
        if(boton_off_sinfin_cont==3){
            if (menu_op==0){
              en_menu=true;
              cont_menu=0;
            //  digitalWrite(SENSOR_IR_ALIM,HIGH);
              Serial.println(F("Comenzando autoconfiguracion del sensor IR.\n Deje el sensor libre 2 segundos"));
              lcd.setCursor(0,0);
              lcd.print(F("Config IR libere:"));
              val_sensor_llenado_a=0;
              auto_config_cont=0;
              menu_op=31;
            }else if(menu_op==34){
              cont_menu=0;
              menu_op=35;
            }else if(menu_op==31 || menu_op==32){
              conf_btn_off=true;
              
            }
        }
      }else{
        boton_off_sinfin_ant=true;
        boton_off_sinfin_cont=0;
      }
    }else{
      boton_off_sinfin_ant=false;
    }

    if(menu_op==31){
      auto_config_cont++;
      val_sensor_llenado_a=analogRead(SENSOR_IR_LLENADO);
      lcd.setCursor(0,1);
      lcd.print(val_sensor_llenado_a);
      lcd.print(F("  confirme"));
      if(conf_btn_off){
        menu_op=32;
        conf_btn_off=false;
        auto_config_cont=0;
        cont_menu=0;
      }
    }else if (menu_op==32){
      auto_config_cont++;
      val_sensor_llenado_c=analogRead(SENSOR_IR_LLENADO);
      lcd.setCursor(0,0);
      lcd.print(F("Config IR tape:"));
      lcd.setCursor(0,1);
      lcd.print(val_sensor_llenado_c);
      lcd.print(F("   confirme"));
      if(conf_btn_off){
        menu_op=33;
        auto_config_cont=0;              
        cont_menu=0;
        val_sensor_llenado=(val_sensor_llenado_a+val_sensor_llenado_c)/2;  
        conf_btn_off=false;
      }
    }else if(menu_op==33){
      auto_config_cont++;
      cont_menu=0;
      lcd.setCursor(0,0);
      lcd.print(F("Valor:"));
      lcd.print(val_sensor_llenado);
      lcd.setCursor(0,1);
      lcd.print(F("OFF confirma"));
      menu_op=34;
      
    }else if(menu_op==34){
      auto_config_cont++;
      if (auto_config_cont>15){
        cont_menu=0;
        auto_config_cont=0;
        menu_op=0;
        lcd.setCursor(0,1);
        lcd.print(F("Abortando"));
        val_sensor_llenado=EEPROM.read(VAL_SENSOR_SUP)*256+EEPROM.read(VAL_SENSOR_INF);
        en_menu=false;
      }
    }else if (menu_op==35){
      EEPROM.update(VAL_SENSOR_SUP,val_sensor_llenado/256);
      EEPROM.update(VAL_SENSOR_INF,val_sensor_llenado%256);
      lcd.setCursor(0,1);
      lcd.print(F("Valor modificado"));
      menu_op=36;
    }else if(menu_op==36){
      menu_op=0;
      en_menu=false;
    }
    if(auto_config_cont==15){
        cont_menu=0;
        menu_op=0;
        auto_config_cont=0;
    }
 ///*********************************************************///
    ///mostrar informacion/////////////////////
    //////************************************************//////
    
    if(!en_menu){//se ejecuta si no se esta mostrando el menu
      if (cont_display<3){
        //Lee e imprime la temperatura y humedad
        float t=dht.readTemperature();
        float h=dht.readHumidity();        
        Serial.print("Temp: ");
        Serial.print(t);
        Serial.println("ºC");
        Serial.print("Hum: ");
        Serial.print(h);
        Serial.println("%");
        if(cont_display==0)lcd.begin(16,2);
        cont_display++;
        mostrarHora();
        lcd.setCursor(0,1);
        lcd.print("T");
        lcd.print((char)223);
        lcd.print(":");
        lcd.print(t,2);
        lcd.print(" H");
        lcd.print((char)223);
        lcd.print(":");
        lcd.print(h,2);
        
       
        Serial.print("Rele 1(Luces)=");
        Serial.println(!digitalRead(RELE_LUCES));
        Serial.print("Rele 2(Sin Fin)=");
        Serial.println(!digitalRead(RELE_SINFIN));
      }else if(estado_bomba==0 || cont_display<6){//cont_display mayor a 3
        cont_display++;
        lcd.setCursor(0,0);
        lcd.print(F("Sinfin:"));
        switch(esta_sinfin){
          case 0:
                lcd.print(F("Func"));
                break;
          case 1:
                lcd.print(F("!Term"));
                break;
          case 2:
                lcd.print(F("Lleno"));
                break;
          case 3:
                lcd.print(F("Apag."));
                break;
          case 4:
                lcd.print(F("NoInic"));
                break;
        }
        lcd.print("   ");
        lcd.setCursor(15,0);
        if (sinfin_modo_manual){
          lcd.print("M");
        }else{
          lcd.print("A");
        }
        lcd.setCursor(0,1);
        lcd.print(F("Luz: "));
        if(!digitalRead(RELE_LUCES)){
          lcd.print(F("ON  "));
        }else{
          lcd.print(F("OFF "));
        }
        if (luces_modo_manual){
          lcd.print(F("Manual"));
        }else{
          lcd.print(F("Auto"));
        }
        if(cont_display>=6 && estado_bomba==0 )cont_display=0;//reinicia el bucle de muestra de informacion
      }else{
        cont_display++;
        if (cont_display>=9)cont_display=0;
        lcd.setCursor(0,0);
        lcd.print(F("Asp. "));
        if (est_rele_bomba){
          lcd.print(F("Enc. "));
          lcd.print(secuencias_bomba[estado_bomba-1][0]-contador_bomba);
          lcd.print(F("    "));
        }else{
          lcd.print(F("Ap. "));
          lcd.print(secuencias_bomba[estado_bomba-1][1]-contador_bomba);
          lcd.print(F("    "));
        }
        lcd.setCursor(0,1);
        lcd.print(secuencias_bomba[estado_bomba-1][0]);
        lcd.print('x');
        lcd.print(secuencias_bomba[estado_bomba-1][1]);
        lcd.print(F("          "));
      }
    }
    


    cada_segundo=false;
  }
  ///*******************************
  //bucle que se ejecuta cada 100 ms
  //***************************
  if (cada_100ms){
    wdt_reset();
    contador_ciclos++;
    if (contador_ciclos==10){
      contador_ciclos=1;
      cada_segundo=true;
      if (en_menu){
        cont_menu++;
        if (cont_menu>9){
          en_menu=false;
          menu_op=0;
          Serial.println(F("Saliendo"));
          lcd.clear();
        }
      }
    }
  
    
  /******************************
  /////////BOTONERA/////////////
  ********************************/
  //reseteo de las variables
  est_boton_on_sinfin=false;
  est_boton_off_sinfin=false;
  est_term_sinfin=false;
  est_boton_luces=false;
  est_sensor_ir_llenado=false;
  est_boton_bomba=false;
  
////botones N/A a masa con pull-up en las entradas activadas
  if (!digitalRead(BOTON_ON_SINFIN)){//boton apretado
    if(ba_boton_on_sinfin){//bandera activa
      est_boton_on_sinfin=true;//activa la entrada
    }else{
      ba_boton_on_sinfin=true;//activa la bandera
    }
  }else{//boton suelto
    ba_boton_on_sinfin=false;//baja la bandera
  }
  if (!digitalRead(BOTON_OFF_SINFIN)){//boton apretado
    if(ba_boton_off_sinfin){//bandera activa
      est_boton_off_sinfin=true;//activa la entrada
    }else{
      ba_boton_off_sinfin=true;//activa la bandera
    }
  }else{//boton suelto
    ba_boton_off_sinfin=false;//baja la bandera
  }
  if (!digitalRead(TERM_SINFIN)){//boton apretado
    if(ba_term_sinfin){//bandera activa
      est_term_sinfin=true;//activa la entrada
    }else{
      ba_term_sinfin=true;//activa la bandera
    }
  }else{//boton suelto
    ba_term_sinfin=false;//baja la bandera
  }
  if (!digitalRead(BOTON_LUCES)){//boton apretado
    if(ba_boton_luces){//bandera activa
      est_boton_luces=true;//activa la entrada
    }else{
      ba_boton_luces=true;//activa la bandera
    }
  }else{//boton suelto
    ba_boton_luces=false;//baja la bandera
  }
  if (!digitalRead(BOTON_BOMBA)){//boton apretado
    if(ba_boton_bomba){//bandera activa
      est_boton_bomba=true;//activa la entrada
    }else{
      ba_boton_bomba=true;//activa la bandera
    }
  }else{//boton suelto
    ba_boton_bomba=false;//baja la bandera
  }

  if(!sinfin_modo_manual && esta_sinfin==0){
    //digitalWrite(SENSOR_IR_ALIM,HIGH);
    if(analogRead(SENSOR_IR_LLENADO)>nivel_sensor){
      if(ba_sensor_ir_llenado){
        est_sensor_ir_llenado=true;
      }else{
        ba_sensor_ir_llenado=true;
      }
    }else{
      ba_sensor_ir_llenado=false;
    }
  }else{
    //digitalWrite(SENSOR_IR_ALIM,LOW);
  }

//Aplicacion de los estados de las entradas en las salidas
////salidas invertidas se activan con LOW y se desactivan con HIGH

  //***********Rele sinfin****************//
  // si rele activo y boton apagar o termico activo o sensor de llenado en modo automatico
//  if(!digitalRead(RELE_SINFIN) && (est_boton_off_sinfin || (!sinfin_modo_manual && est_sensor_ir_llenado) || est_term_sinfin)){
//    digitalWrite(RELE_SINFIN,HIGH);//apaga el rele
//  }else if(est_boton_on_sinfin){//rele desactivado y boton encender
//    digitalWrite(RELE_SINFIN,LOW);//activa el rele
//  }
  if(est_boton_off_sinfin){
    esta_sinfin=3;
  }else if(!sinfin_modo_manual && est_sensor_ir_llenado){
    esta_sinfin=2;
  }else if(est_term_sinfin){
    esta_sinfin=1;
  }else if(est_boton_on_sinfin){
    esta_sinfin=0;
  }
  digitalWrite(RELE_SINFIN,esta_sinfin);
  //**************Rele Luces***********************//
  
  if (est_boton_luces){//boton apretado
    if(boton_luces_suelto){//por primera vez
      if(!luces_modo_manual){//modo automatico
       luces_modo_manual=true;//pasa a modo manual
       if(!digitalRead(RELE_LUCES)){//rele activo
        digitalWrite(RELE_LUCES,HIGH);//Apaga el rele
       }else{//rele apagado
        digitalWrite(RELE_LUCES,LOW);//prende el rele
       }
      }else{//modo manual
        luces_modo_manual=false;//pasa a modo automatico
      }
      boton_luces_suelto=false;
    }
  }else{
    boton_luces_suelto=true;
  }  

  //***************Rele bomba*******************//
  if (est_boton_bomba){//boton apretado
    Serial.print("apre");
    if(boton_bomba_suelto){//por primera vez
      estado_bomba++;
      if(estado_bomba>=5){
        estado_bomba=0;
      }
    }
    boton_bomba_suelto=false;
  }else{
    boton_bomba_suelto=true;
  }
  digitalWrite(RELE_BOMBA,!est_rele_bomba);
  
    cada_100ms=false;
  }//bucle cada 100 ms
  
  //********************************//
  //********Rutina de Menu***********//
  //se usa la bandera en_menu para evitar que se muestre otra informacion 
  //en el puertos serie. un temporizador de 10 segundos se usa entre 
  //ingreso e ingreso para evitar configuraciones inconclusas
  //*********************************//
  if(Serial.available()){
    en_menu=true;
    cont_menu=0;
    //estandarizado de la entrada
    String  str;
    str=Serial.readString(); 
    Serial.println(str);
    if (str.endsWith("\n")){
      
      str.remove(str.length()-1);
//      Serial.println(F("Eliminado Fin de línea"));
      
    } 
    if (str.endsWith("\r")){
      str.remove(str.length()-1);
//      Serial.println(F("Eliminidado Retorno de carro"));
    }
    int int_str=str.toInt();
    str.toUpperCase();

/**menu_op :
0=menu principal
2=mostrar informacion varia
10=modificacion de la hora actual
11=Ingreso de los minutos actuales
12=comprobacion de la modificacion de la hora
20=eleccion del programa a modificar
21=eleccion de la hora de encendido
22= eleccion de los minutos de encendido
23=eleccion de la hora de apagado
24=eleccion de los minutos de apagado
25=confirmacion del programa
1=mostrando programas
31=Iniciando autoconfiguracion del sensor IR de llenado
32=Esperando 2 segundos
33=avisando Bloquear el sensor
34=sensando
35=esperando confirmacion
36=Confirmando


la variable vuelve a 0 cuando rebalsa el cont_menu en el loop cada segundo
**/
    if (menu_op==0){
      if (str=="SETH"){
        menu_op=10;
        Serial.println(F("Modificando la hora del reloj...\nIngrese la hora actual:"));
        lcd.setCursor(0,0);
        lcd.print(F("Modificando Reloj:"));
        lcd.setCursor(0,1);
        lcd.print(F("Hora:   "));
      }else if(str=="VERP"){
        s_programas();
      }else if(str=="MODP"){
        Serial.println(F("Modificar programa..."));
        lcd.setCursor(0,0);
        lcd.print(F("Modif Programa:  "));
        lcd.setCursor(0,1);
        lcd.print(F("Gire y elija:"));
        s_programas();
      Serial.println(F("Ingrese el Nº de programa a modificar:"));
      menu_op=20;
      }else{
        Serial.println(F("Opción inválida. \nIngrese:\nseth para modificar la hora actual.\nverp para ver los programas actuales.\nmodp para modificar un programa."));
      }
    }else if(menu_op==10){
      nueva_hora=-1;
      if (str=="0"){//parsea el string para evitar la conversion del '0' que es el error del metodo toInt()
        nueva_hora=0;
      }else if (int_str<24 && int_str>0){
        nueva_hora=int_str;
      }else{
        s_inv_hora();
      }
      if (nueva_hora>-1){
        Serial.println(F("Ingrese los minutos actuales:"));
        lcd.setCursor(0,1);
        lcd.print(F("Minutos:        "));
        menu_op=11;
      }
        
    }else if(menu_op==11){
      nuevo_minuto=-1;
      if (str=="0"){
        nuevo_minuto=0;
      }else if(int_str<60 && int_str>0){
        nuevo_minuto=int_str;
      }else{
        s_inv_min();
      }
      if (nuevo_minuto>-1){
        Serial.print(F("Se ajustara la hora a: "));
        if(nueva_hora<10){
          hora_lista=String("0")+String(nueva_hora);
        }else{
          hora_lista=String(nueva_hora);
        }
        if(nuevo_minuto<10){
          hora_lista+=String(":0")+String(nuevo_minuto);
        }else{
          hora_lista+=(":")+String(nuevo_minuto);
        }
        Serial.println(hora_lista);
//        Serial.print(nueva_hora);
//        Serial.print(":");
//        Serial.println(nuevo_minuto);
        lcd.setCursor(0,0);
        lcd.print("Nueva hora:"+String(hora_lista));
        s_confirmar();
        menu_op=12;
      }
    }else if (menu_op==12){
     // str.toUpperCase();
      if (str != "N"){
        s_actualizando();
        programarReloj(nueva_hora,nuevo_minuto);
        Serial.println(F("Listo. Se actualizo a:"));
        RTC.getTime();
        mostrarHora();
        lcd.setCursor(0,1);
        lcd.print(F("Hora Actualizada"));
        digitalWrite(RELE_LUCES,controlLuces());
        menu_op=0;
      }else{
        menu_op=0;
        s_abortando();
      }
    }else if(menu_op==20){
      if (int_str<6 && int_str>0){
        num_p=int_str;
        Serial.print(F("Modificando el programa "));
        Serial.println(int_str);
        Serial.println(F("Ingrese la hora de encendido"));
        menu_op=21;
      }else{
        Serial.println(F("Ingrese  un programa válido.(1 a 5)"));
        s_programas();
      }
    }else if(menu_op==21){
      if (int_str<24 && int_str>-1){
        nueva_hora=int_str;
        Serial.println(F("Ingrese el minuto de encendido."));
        menu_op=22;
      }else{
        s_inv_hora();
      }
    }else if(menu_op==22){
      if (int_str>-1 && int_str<60){
        nuevo_minuto=int_str;
        Serial.println(F("Ingrese la hora de apagado"));
        menu_op=23;
      }else{
        s_inv_min();
      }
    }else if(menu_op==23){
      if (int_str>-1 && int_str <24){
        nueva_hora_a=int_str;
        Serial.println(F("Ingrese el minuto de apagado"));
        menu_op=24;
      }else{
        s_inv_hora();
      }
    }else if (menu_op==24){
      if (int_str>-1 && int_str<60){
        nuevo_minuto_a=int_str;
        Serial.print(F("Se modificará el programa "));
        Serial.println(num_p);
        Serial.print("Encendido de ");
        Serial.print(nueva_hora);
        Serial.print(":");
        Serial.print(nuevo_minuto);
        Serial.print(" a ");
        Serial.print(nueva_hora_a);
        Serial.print(":");
        Serial.print(nuevo_minuto_a);
        s_confirmar();
        menu_op=25;
      }else{
        s_inv_min();
      }
    }else if(menu_op==25){
      if (str != "N"){
        programa(num_p).setProg(nueva_hora,nuevo_minuto,nueva_hora_a,nuevo_minuto_a);
        Serial.println(F("Cambios realizados"));
        menu_op=0;
      }else{
        s_abortando();
        menu_op=0;
      }
    }
  }//serial-available

}


    //****************************************************
    //**************************************************
    //FIN LOOP
    //**********************************************************
    //******************************************************



    //*********************************************************
    //envia por puerto serie la hora y fecha actual
    //*********************************************************
void mostrarHora(void) {

   if (RTC.hour < 10)                    // correct hour if necessary
  {
    hora_lista=String("0")+RTC.hour;
    
  }
  else
  {
    hora_lista=String(RTC.hour);
  }
  if(RTC.second%2){
    hora_lista+=':';
  }else{
    hora_lista+=' ';
  }  if (RTC.minute < 10)                  // correct minute if necessary
  {
    hora_lista+=String('0')+RTC.minute;
  }
  else
  {
    hora_lista+=RTC.minute;
  }
   hora_lista+=':';
  if (RTC.second < 10)                  // correct second if necessary
  {
    hora_lista+=String('0')+String(RTC.second);
  }
  else
  {
    hora_lista+=String(RTC.second);
  }
  dia_listo=muestraDia(RTC.dow);
   
  if (RTC.day < 10)                    // correct date if necessary
  {
    dia_listo+=String(" 0")+String(RTC.day);
  }
  else
  {
    dia_listo+=String(" ")+String(RTC.day);
  }
  if (RTC.month < 10)                   // correct month if necessary
  {
   dia_listo+=String("/0")+String(RTC.month);
  }
  else
  {
    dia_listo+=String("/")+String(RTC.month);
  }
  dia_listo+=String('/')+String(RTC.year);
  Serial.println(hora_lista);
  Serial.println(dia_listo);
  lcd.setCursor(0,0);
  lcd.print(hora_lista.substring(0,5)+"  ");
  lcd.print(dia_listo.substring(0,9));


}



//********************************************************
///muestra el nombre del dia segun el numero dom=0-sab=6
//*********************************************************
String muestraDia(unsigned int dow) {
  switch (dow)                      // Friendly printout the weekday
  {
    case 1:
      return String("LUN");
      break;
    case 2:
      return String("MAR");
      break;
    case 3:
      return String("MIE");
      break;
    case 4:
      return String("JUE");
      break;
    case 5:
      return String("VIE");
      break;
    case 6:
      return String("SAB");
      break;
    case 0:
      return String("DOM");
      break;
  }


}

//***********************************************************************************//
//Funcion para programar el reloj segun los datos almacenados en las variables globales
//***********************************************************************************//


void programarReloj() {//para programar con los datos de la compilacion
  Serial.println("Programando reloj...");
  RTC.stopClock();
  RTC.fillByYMD(ano, mes, dia);
  RTC.fillByHMS(hora, minuto, segundo);
  RTC.setTime();
  RTC.startClock();
}
void programarReloj(int p_ano,int p_mes, int p_dia) {//para programar la fecha
  Serial.println("Programando la fecha del reloj...");
  RTC.stopClock();
  RTC.fillByYMD(p_ano,p_mes,p_dia);
  RTC.setTime();
  RTC.startClock();
}
void programarReloj( int p_hora,int p_min){//para programar la hora
    Serial.println("Programando la hora del reloj...");
    RTC.stopClock();
    RTC.fillByHMS(p_hora,p_min,0);
    RTC.setTime();
    RTC.startClock();

}

//******************************************************//
//Funcion que controla el estado de las luces//
//******************************************************//
boolean controlLuces(void){
   boolean salida=0;
   unsigned int hora_actual=RTC.hour*60+RTC.minute;
      for (int i=1;i<6;i++){
        salida=salida || programa(i).estadoLuces(hora_actual);
      }
    return !salida;
}

//**************************************************
//Funciones para dialogos genericos
//*****************************************
void s_confirmar(){
 Serial.println(DESEA_CONFIRMAR);
 lcd.setCursor(0,1);
 lcd.print(DESEA_CONFIRMAR);
}

void s_actualizando(){
 Serial.println(ACTUALIZANDO);
 lcd.setCursor(0,1);
 lcd.print(ACTUALIZANDO);
}

void s_abortando(){
 Serial.println(ABORTANDO);
 lcd.setCursor(0,1);
 lcd.print(ABORTANDO);
}

void s_programas(){
  for (int i=1;i<6;i++){
   programa(i).serial();
  }
}

void s_inv_hora(){
  Serial.println(INGRESE_HORA );
 lcd.setCursor(0,1);
 lcd.print(INGRESE_HORA);
}

void s_inv_min(){
  Serial.println(INGRESE_MINUTOS);
 lcd.setCursor(0,1);
 lcd.print(INGRESE_MINUTOS);
}
