# control_galpon
Control de un galpon con Arduino 

Con este Firmware se busca controlar un galpon de produccion de gallinas ponedoras con los siguientes accesorios:
·sinfin de llenado del comedero(acciona un contactor y se detiene al sensar el comedero lleno con un sensor IR)
·Luces ambientes(mediante programacion de horarios)

--Proximamente--
·Ventiladores( se encenderan segun la temperatura ambiente medida por varios dht 11)
·Sistema de neblina fogger(accionara la bomba intermitentemente segun la Tº y la Hº medida)
·comunicacion con otros modulos para un control manual o para control redundante mediante nrf 24l01 o similar

Funciona testeado en ARDUINO MEGA 2560.

Lista de materiales:
·Arduino Mega 2560
·RTC ds3231 (cualquier modulo es compatible)
·Modulo de Relés (Necesarios 2 Relés)
·Sensor IR de tipo Barrera(según se describe en el esquema de Fritzing Adjunto)
·2 pulsadores(1 N/a y 1 N/c)
·Fuente de alimentación de 5V/2A
·Fuente de alimentación de 9-15V/1A (Opcional para alimentar independientemente la placa Arduino)
